# A Drop from the Bucket Blog

A Rust-driven, Markdown-based static site generator for simple, twitter-sized blog posts

## Directory Structure
```
## Project Structure
- public/
  - archive/             # Archive of never-posted, or abandoned materials
  - assets/              # Static assets, pngs, etc.
  - feeds/               # RSS Feeds
  - index.html           # Simple home page
  - links/               # archive of html + md files for links page
  - pages/               # Static, never-changing, or manually changed pages
  - posts/               # drafts, posted MD and posted html file for blog posts
  - scripts/             # Rust and bash scripts for control
  - styles.css           # Stylesheet for the entire blog
- LICENSE
- README.md
```

## Features:
*What it can do, simply*
 - RSS Feed
 - PGP Singing built into every blog post
 - Categorical Tags: Drop(<100 words), Splash(100-500 words), Torrent(500+ words)
 - Simple CLI for creating & posting blogs

## Anti-Features:
*Purposefull feature omissions, for sanity's sake*
 - No DB integration, CMS entirely document-based
 - No User Authentication, commenting, or user interaction
 - One CSS Stylesheet
 - No real-time updates
 - Limited 3rd Party Dependancies
 - No Rich Media Integration
 - Limited file type allowance: 
    - .sh, .rs     # Scripting, Rust is primary, bash is backup
    - .md          # Writing blogs
    - .html        # web content
    - .css(1)      # one stylesheet
    - .xml         # for RSS Feeds

## Usage
The following are the valid commands, congiured in the .bashrc
+ `blog new ["Title"]`   - `blog new "Don't Read This Post"`
+ `blog list`            - `blog list`
+ `blog edit [$num]`     - `blog edit 0012`
+ `blog sign [$num]`     - `blog sign 0012`
+ `blog post [$num]`     - `blog post 0012`
+ `blog publish [$num]`  - `blog publish 0001`

NOTE: `blog publish` functionality is meant to automate `git add .`, `git commit -m "posted [####]"`, and `git push`. Currently on the backburner 

## License
Unlicense - I am not liable for this spagetti code

## TODOS
 - Script documentation & exception handling
 - DNS Redirects to simplify `/posts/html/*` to `/*`?
 - Make generic library for others to use and adapt Rust scripts and static blog site generator. No great strategy for handling media though...

