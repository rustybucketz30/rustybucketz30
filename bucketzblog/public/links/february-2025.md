## pilled-texts
- [We are destroying software](https://antirez.com/news/145)
- [Story of Civilization Quotes](https://jarbus.net/blog/story-of-civilization-1-quotes/)
- [50 Years of Travel Trips](https://kk.org/thetechnium/50-years-of-travel-tips/)
- [The Murderer](http://www.sediment.uni-goettingen.de/staff/dunkl/zips/The-Murderer.pdf)

## foss-stuff
- [Open Data for AI](https://gr.inc/)

## ui-stuff
- [JavaScript Kaleidoscope](https://codepen.io/AAMutlu20/pen/mdYxroj)

## ai-stuff
- [IBM Granite](https://www.ibm.com/granite/docs/)
- [Local AI Voice Cloner](https://github.com/Zyphra/Zonos)
- [AI Development plan](https://harper.blog/2025/02/16/my-llm-codegen-workflow-atm/)

## learning-tutorials
- [Auto-Crafting Item Filter](https://www.youtube.com/watch?v=qaEsE8bLUNA)
- [Google's titan AI Architecture](https://medium.com/@sahin.samia/google-titans-model-explained-the-future-of-memory-driven-ai-architectures-109ed6b4a7d8)

## interesting-blogs
- [Proompting Tips](https://simonwillison.net/2025/Feb/2/openai-reasoning-models-advice-on-prompting/#atom-everything)
- [What is a "better" model?](https://www.ben-evans.com/benedictevans/2025/1/the-problem-with-better-models)
- [A computer can never be held accountable](https://simonwillison.net/2025/Feb/3/a-computer-can-never-be-held-accountable/#atom-everything)
- [Animating Rick and Morty](https://simonwillison.net/2025/Feb/4/animating-rick-and-morty-one-pixel-at-a-time/#atom-everything)
- [Startup Advice - All about listening](https://longform.asmartbear.com/put-down-the-compiler/)
- [Vendor Slurry](https://simonwillison.net/2025/Feb/5/ai-generated-slop-is-already-in-your-public-library/#atom-everything)
- [I agree, labels are dumb af](https://manuelmoreale.com/identity)
- [Weird LLM Take](https://simonwillison.net/2025/Feb/6/the-future-belongs-to-idea-guys-who-can-just-do-things/#atom-everything)
- [F 'Success'](https://longform.asmartbear.com/expert-distraction/)
- [Normals are all different](https://manuelmoreale.com/context)
- [Ted on Idiots living forever](https://www.honest-broker.com/p/how-to-achieve-immortality)
- [Business and Luck](https://longform.asmartbear.com/copy-number-one/)
- [Based Question hidden here](https://rxjourney.com.ng/trapped-in-the-matrix)
- [Opportunity Cost](https://rxjourney.com.ng/success-at-what-cost)
- [Contradictory Wisdom](https://rxjourney.com.ng/when-did-i-start-caring-about-money-a-personal-exploration)
- [Digital Tinnitus/Micro-aggressions lol](https://www.wheresyoured.at/what-were-fighting-for/)
- [Mastery](https://so1o.xyz/blog/white-light)
- [Yes, literally BASED](https://manuelmoreale.com/confidently-incorrect)
- [Music is not healthy](https://www.honest-broker.com/p/the-music-business-is-healthy-again)
- [The Flow State](https://www.honest-broker.com/p/how-we-lost-the-flow)
- [Good Startup Marketing Advice, but can't act on it](https://longform.asmartbear.com/first-marketing-channel/)
- [AI is a ticking time bomb](https://www.wheresyoured.at/longcon/)
- [Like Rome and Sears before it, Google will fall](https://www.honest-broker.com/p/the-worlds-largest-search-doesnt)
- [Whoa Derek](https://sive.rs/married)
- [Using bots to prop up population decline and capitalism?](https://kk.org/thetechnium/the-handoff-to-bots/)
- [Mathematical mucisians](https://www.honest-broker.com/p/classical-music-got-invented-with)
- [Derek's Not Failed Relationship](https://sive.rs/anna)

## hn-discussions
- [Blackpilling on Healthcare](https://news.ycombinator.com/item?id=42992121)
- [Why blog? Yes](https://news.ycombinator.com/item?id=42992159)
- [HackerRank Hate](https://news.ycombinator.com/item?id=12667174)

## random-funny
- [Google Buidling Weaponds](https://www.youtube.com/watch?v=-v6M_MEbkbI)
- [7 Hermetic Principles](https://www.mindbodygreen.com/articles/7-hermetic-principles)
- [Godfather of the CIA, nuthead](https://en.wikipedia.org/wiki/Allen_Dulles)
- [Cheapest Destination Blog](https://www.cheapestdestinationsblog.com/)
- [Hermeticism](https://en.wikipedia.org/wiki/Hermeticism)
- [AI Pin Shutdown FAQs, hilarious](https://support.humane.com/hc/en-us/articles/34243204841997-Ai-Pin-Consumers-FAQ)
- [210 Reasons for the Fall of Rome](https://courses.washington.edu/rome250/gallery/ROME%20250/210%20Reasons.htm)
- [An indian mirror - lol](https://www.bugswriter.com/)
- [All .gov websites](https://flatgithub.com/cisagov/dotgov-data/blob/main/?filename=current-full.csv&sha=7dc7d24fba91f571692112d92b6a8fbe7aecbba2)
