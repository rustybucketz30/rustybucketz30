## pilled-texts
- [The Meaning of Life](https://sive.rs/ml)
- [The Cheap Web](https://potato.cheap/)

## foss-stuff
- [BeeperMini - #rd party iMessage Client](https://blog.beeper.com/p/beeper-mini-is-back)
- [FLOSS VM Image Generator - Packer](https://mitchellh.com/writing/packer)
- [FLOSS USB Bootable Formatter](https://www.ventoy.net/en/index.html)
- [FOSS Wireframing Tool - Wireflow](https://wireflow.co/)

## ui-stuff
- [Solid, simple Personal Blog](https://eli.li/)
- [Weird, but fun and funky personal website](https://www.kickscondor.com/)
- [I like these gradients](https://knock.app/blog/zero-downtime-postgres-upgrades)
- [Kali Linux Website UI is based](https://www.kali.org)
- [TailwindCSS - Catalyst](https://tailwindui.com/templates/catalyst)
- [Very Beautiful Tech Website](https://adaptive.live/)
- [Web Design in 4 minutes](https://jgthms.com/web-design-in-4-minutes/)

## ai-stuff
- [FOSS Azure LLM Management (BricksLLM)](https://github.com/bricks-cloud/BricksLLM)
- [MacOS AI Assistant](https://github.com/elfvingralf/macOSpilot-ai-assistant)
- [Small Offline LLM for M1 Macs](https://github.com/mit-han-lab/TinyChatEngine)
- [Self-prompting AI Bot](https://github.com/aymenfurter/microagents)

## learning-tutorials
- [High-level overview of EXT4 file system](https://metebalci.com/blog/a-minimum-complete-tutorial-of-linux-ext4-file-system/)
- [The Rust Programming Language Book](https://doc.rust-lang.org/stable/book/)
- [Technical Writing from the IEEE](https://ias.ieee.org/wp-content/uploads/2023/06/2020-01-16_IET_Technical_Report_Writing_Guidelines.pdf)

## interesting-blogs
- [Nat Friedman Blog Page](https://nat.org)
- [Sigil Wen Website Page](https://sigilwen.ca)
- [Old Blogger with High-level Writeups](https://carlos.bueno.org/all.html)
- [StuffMadeHere Blog](https://shane.engineer/about)
- [Blogger to Look into](https://xeiaso.net/)
- [Self-hosted Everything](https://tube.jeena.net/w/nivehRx8J7ZwujfS2oCoPC)
- [Tips to Being Exceptional](https://sigilwen.ca/exceptional.html)
- [Tips to Being Productive](https://sigilwen.ca/productivity.html)
- [Great Questions](https://sigilwen.ca/questions.html)
- [AI Will Enable Mass Spying](https://slate.com/technology/2023/12/ai-mass-spying-internet-surveillance.html)
- [Philosophy on Documentation](https://eli.li/docs)
- [Wishlists](https://taylor.town/wish-manifesto)
- [The Concept of God in Santa Claus](https://taylor.town/santa-claus-god)
- [The End is Nigh, NOT](https://www.programmablemutter.com/p/the-singularity-is-nigh-republished)
- [You Don't Batch Cook When You're Suicidal](https://cookingonabootstrap.com/2020/07/30/the-price-of-potatoes-and-the-value-of-compassion/)
- [Micro Vs Macro Culture - War incoming?](https://www.honest-broker.com/p/in-2024-the-tension-between-macroculture)
- [Freud History](https://archive.ph/NW95o)
- [Dive into Pedagogy and Free Inquiry](https://naturalselections.substack.com/p/uatxresignation)
- [Ranking University Free Speech?](https://www.thefire.org/)
- [Unorthodox Learning Nonprofit](https://heterodoxacademy.org/about/)
- [A Walk and Talk](https://sive.rs/wt)
- [GNU Manifesto](https://www.gnu.org/gnu/manifesto.html)
- [19 Lessons for Writing Good Software](https://en.wikipedia.org/wiki/The_Cathedral_and_the_Bazaar)
- [Lucidity being Lucidity](https://ludic.mataroa.blog/blog/tech-management-has-bestowed-glory-upon-me/)
- [FAQs](https://sive.rs/faq)
- [Disconnect](https://sive.rs/dc)
- [Autmoation Obsessed](https://mitchellh.com/writing/automation-obsessed)
- [A.P.P.L.E. User Interaction Strategy](https://mitchellh.com/writing/apple-the-key-to-my-success)
- [Startups are an act of desperation](https://blog.eladgil.com/p/startups-are-an-act-of-desperation)
- [Customer Service is a profit center](https://sive.rs/cs1)
- [VCSpeak](https://jacobbartlett.substack.com/p/yes-actually-means-no-the-curious)
- [Propheteering from the Altman](https://blog.samaltman.com/the-merge)
- [Yeah, i feel that - me different](https://sive.rs/wrong)
- [Personal Project Hell](https://siddhesh.substack.com/p/projects)
- [Ludicity 2023 Blog Ender](https://ludic.mataroa.blog/blog/merry-christmas-ya-filthy-animals-2023/)
- [Nice Email from Pardoned Junior Developer](https://taylor.town/pardoned)
- [Why Bash was replaced with zsh on Mac](https://discussions.apple.com/thread/250729585?sortBy=best)
- [Web Development History](https://webdevelopmenthistory.com/index/)
- [Is it tech obsolescence or mine that is being planned?](https://walterkirn.substack.com/p/generation-junk)
- [Self-Hosting Email Giving Up Blog Post](https://cfenollosa.com/blog/after-self-hosting-my-email-for-twenty-three-years-i-have-thrown-in-the-towel-the-oligopoly-has-won.html#note-2-back)
- [Another Email Warning](https://www.attejuvonen.fi/dont-send-email-from-your-own-server/)
- [Write Cold-Blooded Software](https://dubroy.com/blog/cold-blooded-software/)
- [Search Engine History and Comparison](https://danluu.com/seo-spam/)
- [Cypherpunk Manifesto](https://www.activism.net/cypherpunk/manifesto.html)

## hn-discussions
- [Fediverse Discussion](https://news.ycombinator.com/item?id=38511346&p=2)
- [Free Will Discussion](https://news.ycombinator.com/item?id=38458470)
- [Google Loses in Court to Epic](https://news.ycombinator.com/item?id=38607424)
- [Discussion on Freud](https://news.ycombinator.com/item?id=38619724)
- [HN Servers Discussion](https://news.ycombinator.com/item?id=38710318)
- [HN Discussion on Neoliberalism](https://news.ycombinator.com/item?id=38744612)
- [Copyright is not a moral right](https://news.ycombinator.com/item?id=38803614)
- [HN Email Hosting Discussion](https://news.ycombinator.com/item?id=38790854)
- [HN Email Self-Hosting Discussion](https://news.ycombinator.com/item?id=38796078)

## random-funny
- [Interesting Framework for Documentation](https://luke.hsiao.dev/blog/housing-documentation/)
- [Beeper Mini/Cloud - iMessage 3rd party client](https://news.ycombinator.com/item?id=38531759)
- [How Did I Get Here?](https://how-did-i-get-here.net)
- [DynamicLand - Modern Computing - physical interaction](https://dynamicland.org/)
- [Public Domain Manifesto](https://publicdomainmanifesto.org/)
- [Mozilla Memory Cache](https://future.mozilla.org/blog/introducing-memorycache/)
- [Ad Free Cities](https://adfreecities.org.uk/about/faqs/)
- [Pro Excel Player - Hilarious](https://www.youtube.com/watch?v=xubbVvKbUfY)
- [AI Boyfreind](https://www.youtube.com/watch?v=KiPQdVC5RHU)
- [M$ Promptbase Code](https://github.com/microsoft/promptbase)
- [nownownow.com](https://nownownow.com/)
- [Let us all unite!](https://www.youtube.com/watch?v=ouzKl0oD6sU)
- [Poe's law](https://en.wikipedia.org/wiki/Poe's_law)
- [AI Doomerism Artcile about Agency](https://www.lesswrong.com/posts/kpPnReyBC54KESiSn/optimality-is-the-tiger-and-agents-are-its-teeth)
- [investigate IRC clients](https://oftc.net/)
- [Wordle-solving robot using 3D printer](https://www.youtube.com/watch?v=_QHz_5pqPuo)
- [GameBoy Music Archive](https://www.vgmusic.com/music/console/nintendo/gameboy/)
- [LackRack](https://web.archive.org/web/20230131031301/https://wiki.eth0.nl/index.php/LackRack)
- [WTF Happened in 1971](https://wtfhappenedin1971.com/)
- [Weeks of My Life](https://www.weeksofyour.life/?2001-04-01=4%2520in%25202001&2000-04-02=3%2520in%25202000&birthdate=2000-06-04)
- [Interesting License (SSPL)](https://www.sspl.org/)
- [Email Spam Filtering](https://www.rspamd.com/)
- [Coders at Work book](https://codersatwork.com/)
- [Dark Visitors - Ai Crawling Bots](https://darkvisitors.com/)
- ["privacy centric" Analytics](https://usermaven.com/pricing)
- [Favorited HackerNews posts](https://observablehq.com/@tomlarkworthy/hacker-favourites-analysis)

