## pilled-texts
- [Directions, not goals](https://www.lesswrong.com/posts/uwmFSaDMprsFkpWet/explore-more-a-bag-of-tricks-to-keep-your-life-on-the-rails)
- [Being Humble cannot fail](https://longform.asmartbear.com/pascals-wager/)
- [Up Vs. Down](https://www.honest-broker.com/p/15-observations-on-the-new-phase)

## foss-stuff
- [TruffleHog - Credential finder](https://github.com/trufflesecurity/trufflehog)
- [Proxmox VE Scripts](https://community-scripts.github.io/ProxmoxVE/)
- [Dumb File Server](https://github.com/sigoden/dufs)
- [CLI Benchmarker](https://github.com/sharkdp/hyperfine)
- [CLI Find alternate](https://github.com/sharkdp/fd)
- [Supercharged cat as bat](https://github.com/sharkdp/bat)
- [tldr man pages](https://github.com/tealdeer-rs/tealdeer)
- [Stack Analyzer](https://github.com/specfy/stack-analyser)
- [Bocker - Docker implemented in bash](https://github.com/p8952/bocker)

## ui-stuff
- [HTML to React + Figma in the Chrome Web Store](https://chromewebstore.google.com/detail/html-to-react-figma-by-ma/chgehghmhgihgmpmdjpolhkcnhkokdfp)
- [AI Web Design to HTML + Tailwind](https://tips.io/)
- [Redesigning Blog](https://so1o.xyz/blog/v1.1a)
- [100 Visualizations from 1 dataset](https://100.datavizproject.com/)
- [Frosted Glass Effect](https://www.tyleo.com/html-glass.html)
- [OpenStreetMap iframe](https://simonwillison.net/2024/Nov/25/openstreetmap-embed-url/#atom-everything)

## ai-stuff
- [AI as a Judge](https://simonwillison.net/2024/Oct/30/llm-as-a-judge/#atom-everything)
- [All This guy has learned about local LLMs](https://nullprogram.com/blog/2024/11/10/)
- [Image Model better than Midjourney?](https://simonwillison.net/2024/Nov/15/recraft-v3/#atom-everything)
- [Running Pixtral Large](https://simonwillison.net/2024/Nov/18/pixtral-large/#atom-everything)
- [Document Understanding Transformer](https://github.com/clovaai/donut)

## learning-tutorials
- [Wikiman - offline wikis](https://github.com/filiparag/wikiman)
- [Networking Fundamentals](https://iximiuz.com/en/series/computer-networking-fundamentals/)

## interesting-blogs
- [Single Most Important SaaS metric](https://longform.asmartbear.com/saas-metric/)
- [Tech Support = Sales](https://longform.asmartbear.com/tech-support-is-sales/)
- [M$s Cult Mindset](https://www.wheresyoured.at/the-cult-of-microsoft/)
- [Manu Losing Focus, me too bro](https://manuelmoreale.com/regaining-focus)
- [Crisis of Seriousness](https://www.honest-broker.com/p/is-there-a-crisis-of-seriousness)
- [How do you live?](https://so1o.xyz/blog/how-do-you-live)
- [Why am I learning about meth](https://dynomight.net/p2p-meth/) 
- [Homelab Reflections](https://mtlynch.io/building-a-vm-homelab/)
- [Year 6 as a bootstrapped founder - read all 6](https://mtlynch.io/solo-developer-year-6/)
- [This guy selling TinyPilot](https://mtlynch.io/i-sold-tinypilot/)
- [Cybertruck and Depressing Design](https://www.honest-broker.com/p/why-is-the-tesla-cybertruck-so-depressingly)
- [Cuddling What I used to kill](https://sive.rs/rats)
- [Using Plex for Metadata of Songs](https://coryd.dev/posts/2024/importing-music-metadata-from-plex)
- [Machines of Loving Grace](https://darioamodei.com/machines-of-loving-grace)
- [Legally Owning Another Person](https://medium.com/incerto/how-to-legally-own-another-person-4145a1802bf6)
- [Celebrating Smallness](https://manuelmoreale.com/small-scale-is-the-best-scale)
- [Questions are consideration, not refutations](https://sive.rs/qcc)
- [New Rules of Communication](https://www.honest-broker.com/p/the-6-new-rules-of-communicating)
- [Eudamonia in the Information Age](https://jarbus.net/blog/eudaimonia-in-the-information-age/)
- [P-Hacking Epidemic](https://longform.asmartbear.com/p-hacking/)
- [Lost in the Future](https://www.wheresyoured.at/lost-in-the-future/)
- [Dismissed](https://sive.rs/dis)
- [Building Junk Page on Internet](https://taylor.town/junk-guide)
- [BingAI going haywire](https://simonwillison.net/2024/Nov/19/notes-from-bing-chat/#atom-everything)
- [History on Passwords](https://simonwillison.net/2024/Nov/21/password-policies/#atom-everything)
- [Same, I don't get it lol](https://manuelmoreale.com/media-diet)
- [Entrepreneurship Facts](https://simonwillison.net/2024/Nov/23/james-dillard/#atom-everything)
- [Repair and Maintain over Build](https://www.notechmagazine.com/2024/11/field-notes-repair.html)
- [Insurance Insurance](https://taylor.town/insurance)
- [Idea Kitty](https://taylor.town/idea-kitty)

## hn-discussions
- [SciAm Editor Bias Discussion](https://news.ycombinator.com/item?id=42177619)
- [Based Hn Commentor](https://news.ycombinator.com/threads?id=akomtu)

## random-funny
- [Floor Sitting? Kinda cool](https://www.youtube.com/watch?v=L_9oU88UH_I)
- [Pissed about politics](https://www.youtube.com/watch?v=3j7m0tbZJgE)
- [ZenPen - Quotes/Wisdom](https://www.zenpen.club/)
- [Ping - Uptime Checker](https://ping.barelyhuman.dev/)
- [Elon Musk Today](https://elonmusk.today/)
- [Hera, The Goddess of Marriage](https://www.psychologytoday.com/us/blog/stronger-the-broken-places/201803/hera-the-greek-goddess-marriage)
- [Andre De Shields Quotes](https://www.andredeshields.com/quotes)
- [Academic Standard Technologies + Certifications](https://www.1edtech.org/)
- [With Great Power comes Greater Fear](https://en.wikipedia.org/wiki/Damocles)
- [Nootropics](https://www.astralcodexten.com/p/link-troof-on-nootropics)
- [Query SQLite or CSV/JSON from CLI](https://simonwillison.net/2024/Nov/25/ask-questions-of-sqlite/#a)

