## pilled-texts


## foss-stuff
- [FOSS Google Docs (etherpad)](https://etherpad.org/)
- [FOSS Google Docs (Collbora)](https://www.collaboraoffice.com/)
- [FOSS Read-it-later app](https://omnivore.app/)
- [FOSS Data](https://ourworldindata.org/)
- [FOSS Multimodal LLM](https://llava.hliu.cc/?ref=blog.roboflow.com)
- [FOSS Hardware _ libre Silicon](https://libresilicon.com/)
- [FOSS Online, Offline Planetarium (awesome)](https://www.ivoyager.dev/app/planetarium.html)
- [FOSS React Code Writer with GPT-4 API & Figma](https://reactagent.io/)

## ui-stuff
- [Nice Blog UI (Scott Spence](https://scottspence.com)
- [CSS Background Animation](https://codepen.io/quadbaup/full/ZEQqzqM)
- [Picture to Color Palette](https://coolors.co/image-picker)
- [Clean UI - Fork for blogs?](https://www.fermyon.com/blog/rethinking-open-source-licenses)
- [QX82 - Retro Gaming JS Engine](https://btco.github.io/qx82/)
- [Kitty Terminal Themes](https://github.com/kovidgoyal/kitty-themes/blob/master/themes/LiquidCarbonTransparent.conf)
- [Nice, Dark, Blog UI](https://effectiveaccelerationism.substack.com/)
- [AI Animations](https://animatedai.github.io/)

## ai-stuff
- [ControlNet](https://github.com/lllyasviel/ControlNet)

## learning-tutorials


## interesting-blogs
- [You are where you work - career advice](https://moxie.org/2013/01/07/career-advice.html)
- [Brutal US Experimentation](https://lithub.com/how-us-intelligence-agencies-hid-their-most-shameful-experiments/)
- [HackerNews Book Reccomendations](https://blog.reyem.dev/post/extracting_hn_book_recommendations_with_chatgpt_api/)
- [Tech doesn't make our lives easier, just faster](https://brettscott.substack.com/p/tech-doesnt-make-our-lives-easier)
- [Five types of Friends & Love](http://sociological-eye.blogspot.com/2022/02/five-kinds-of-friends.html)
- [How to manage 200+ FOSS projects](https://turbot.com/blog/2023/10/repo-management)
- [11 years of SaaS product development](https://ghiculescu.substack.com/p/11-years-of-saas-product-strategy)
- [What's wrong with Patreon.](https://siderea.dreamwidth.org/1824441.html)
- [Your Organization doesn't want to improve things](https://ludic.mataroa.blog/blog/your-organization-probably-doesnt-want-to-improve-things/)
- [Discussion of Crime (NYC)](https://marginalrevolution.com/marginalrevolution/2023/04/new-york-city-fact-of-the-day-5.html)
- [Elm at Ratuken](https://engineering.rakuten.today/post/elm-at-rakuten/)
- [The Python Paradox](http://www.paulgraham.com/pypar.html)
- [I will dropkick you if you use that spreadsheet](https://ludic.mataroa.blog/blog/i-will-fucking-dropkick-you-if-you-use-that-spreadsheet/)
- [Values Over Discipline](https://taylor.town/choose-values-over-discipline)
- [The only _good_ use for Ring Cameras](https://www.bbc.com/news/technology-67053171)
- [My 12 Favorite Problems](https://www.honest-broker.com/p/my-12-favorite-problems)
- [The Cottage Computer Programmer](https://www.atariarchives.org/deli/cottage_computer_programming.php)
- [Agile Can Suck Me](https://ludic.mataroa.blog/blog/i-will-fucking-haymaker-you-if-you-mention-agile-again/)
- [Write More Useless Software](https://ntietz.com/blog/write-more-useless-software/)
- [Two Firefox Profiles](https://utcc.utoronto.ca/~cks/space/blog/web/FirefoxExtraProfilesHack)
- [Consumerism Angst](https://arachnoid.com/lutusp/consumerangst.html)
- [Startup CTO Handbook](https://github.com/ZachGoldberg/Startup-CTO-Handbook/blob/main/StartupCTOHandbook.md)
- [Software Disenchantment](https://tonsky.me/blog/disenchantment/)
- [Baloney detection system - Carl Sagan](https://www.themarginalian.org/2014/01/03/baloney-detection-kit-carl-sagan/)
- [A Visit to the Internet Archive](https://thenewstack.io/a-visit-to-the-physical-internet-archive/)
- [12 Predictions on the Future of Song Investing](https://www.honest-broker.com/p/12-predictions-on-the-future-of-song)
- [Moving Off GitHub](https://ntietz.com/blog/moving-off-github/)
- [Taming your infinite Queues](https://taylor.town/infinite-queues)

## hn-discussions
- [Discussion on US Experimentation](https://news.ycombinator.com/item?id=37766711)
- [Does Technology Actually Make our lives Easier or just repalce it with more work](https://news.ycombinator.com/item?id=37767335)
- [Discussion on Jobs, BigCo & SmallCo](https://news.ycombinator.com/item?id=37806870)
- [The Techno-Optimist Manifesto](https://news.ycombinator.com/item?id=37899993)
- [Windows to Show Ads in OS-default Mail client](https://news.ycombinator.com/item?id=38004709)
- [Private Equity Discussion](https://news.ycombinator.com/item?id=38069197&p=2)
- [Microservices Discussion](https://news.ycombinator.com/item?id=38069915)

## random-funny
- [WWOOFing? Farm Life](https://wwoofusa.org/en/hosts)
- [Elm, non-JS front-end web framework](https://guide.elm-lang.org/interop/)
- [American Time Use Survey](https://www.bls.gov/tus/tables/a1-2022.pdf)
- [Momo (novel) - Time thieves](https://en.wikipedia.org/wiki/Momo_(novel))
- [Paul Virilio](https://en.wikipedia.org/wiki/Paul_Virilio)
- [Beware of Moloch](https://www.youtube.com/watch?v=NOie4GCI40I)
- [Bitmagnet - BitTorrent with DHT Crawler](https://bitmagnet.io/)
- [Replacing propreitary JS with FOSS - Haketilo](https://haketilo.koszko.org/)
- [Clean Linux Setup](https://www.youtube.com/watch?v=nNvciN4sGKQ&t=69s)
- [Awesome game performance in browser (120fps)](https://dotbigbang.com/game/1af877e9bfdb47088611f55982b7570f/prestons-diamond-wars?mp=playdw)
- [TaskWarrior TUI](https://github.com/kdheepak/taskwarrior-tui)
- [Reaper DAW](https://www.reaper.fm/)
- [Code Golf](https://en.wikipedia.org/wiki/Code_golf)
- [List of 'awesome' stuff](https://taylor.town/awesome-awesome-awesome)
- [Bus Factor](https://en.wikipedia.org/wiki/Bus_factor)
- [F5 Bot - Keyword Notification System](https://f5bot.com)
- [The Twelve Networking Truths](https://www.rfc-editor.org/rfc/rfc1925)
- [Email + Git = <3](https://git-send-email.io/)
- [Metric Time - Let's Switch](https://metric-time.com/)
- [How to Draw an Owl](https://knowyourmeme.com/memes/how-to-draw-an-owl)
- [PHONK](https://www.youtube.com/watch?v=hhOQT693q00)
- [How to Leave Internet Comments](https://www.youtube.com/watch?v=x_QmvZRS85U)
- [The Snowden Archive](https://github.com/iamcryptoki/snowden-archive)
- [Linux Performance Tools](https://www.brendangregg.com/linuxperf.html)
- [Alternative Frontend for YouTube](https://github.com/d3vr/yt-siphon)
- [Visualizing American Inflation](https://perthirtysix.com/tool/visualizing-american-inflation)
- [Jet Packs](https://gravity.co/)
- [Easy or Cheap Fast API Hosting](https://fly.io/)
- [AI Powered RSS Feed](https://www.youtube.com/watch?v=EdVXmpyMD1k)
- [PyPipe - CLI for pipeline processing](https://github.com/bugen/pypipe)
- [Linux Kernel - VFIO for COD Gaming?](https://www.kernel.org/doc/html/latest/driver-api/vfio.html)
- [Shit is the most complicated English word - Comedy Skit](https://www.youtube.com/watch?v=igh9iO5BxBo)
- [Kill Me - Chrome is going to pay for people's cookies pretty soon](https://www.reddit.com/r/beermoney/comments/14pvngu/anyone_been_invited_to_google_online_insights/)
- [VimWiki](https://vimwiki.github.io/)
- [Brax.me, interesting project](https://brax.me/)
- [How does this guy only have like 600 subs](https://www.youtube.com/@MAKiTHappen/videos)
- [Super Based Video, Geez](https://www.youtube.com/watch?v=ChKpf5HjcSY)
- [Find God - lol](https://www.youtube.com/watch?v=BdMf02eQFr)
