#!/bin/bash

# Define the absolute path to your Rust project directory
rust_project_dir="/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/scripts/"

if [ "$#" -lt 1 ]; then
    echo "Usage: blog {new|edit|publish|list|post|update-links|sign} [...]"
    exit 1
fi

command=$1

cd "$rust_project_dir"

# Create a temporary file for error output
temp_file=$(mktemp)

# Function to run Cargo command quietly and handle errors
run_cargo_quietly() {
    if ! cargo run --quiet --bin "$command" "$@" 2> "$temp_file"; then
        cat "$temp_file"
    fi
}

# Execute the appropriate Rust script based on the command
case "$command" in
    new)
        echo "Creating a new post..."
        shift
        run_cargo_quietly "$@"
        ;;
    edit)
        echo "Editing post..."
        shift
        run_cargo_quietly "$@"
        ;;
    publish)
        if [ "$#" -eq 2 ]; then
            blog_number=$2
            echo "Signing and posting blog ${blog_number}..."

            # Sign the blog
            if ! cargo run --quiet --bin "sign" "$blog_number" 2> "$temp_file"; then
                cat "$temp_file"
                exit 1
            fi

            # Post the blog
            if ! cargo run --quiet --bin "post" "$blog_number" 2> "$temp_file"; then
                cat "$temp_file"
                exit 1
            fi

            # Git commands to add, commit, and push
            echo "Adding, committing, and pushing blog ${blog_number} to Git..."
            git add .
            git commit -m "posted blog ${blog_number}"
            git push

        else
            echo "Usage: blog publish <blog_number>"
            exit 1
        fi
        ;;
    post)
        echo "Processing post..."
        shift
        run_cargo_quietly "$@"
        ;;
    update-links)
        echo "Updating links..."
        shift
        run_cargo_quietly "$@"
        ;;
    sign)
        if [ "$#" -eq 2 ]; then
            echo "Signing draft ${2}..."
            shift
            # Call the Bash script directly
            if ! ./sign.sh "$1" 2> "$temp_file"; then
                cat "$temp_file"
                exit 1
            fi
        else
            echo "Usage: blog sign <file_number>"
            exit 1
        fi
        ;;
    list)
        run_cargo_quietly
        ;;
    *)
        echo "Invalid command. Usage: blog {new|edit|publish|list|post|update-links|sign} [...]"
        exit 1
        ;;
esac

rm -f "$temp_file"

