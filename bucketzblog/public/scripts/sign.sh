#!/bin/bash

# Function to count words in a string
count_words() {
    local content="$1"
    echo "$content" | wc -w
}

# Function to determine the tag based on word count
determine_tag() {
    local word_count="$1"
    if [ "$word_count" -le 256 ]; then
        echo "drop"
    elif [ "$word_count" -le 512 ]; then
        echo "splash"
    else
        echo "torrent"
    fi
}

add_tag_to_draft() {
    local draft_content="$1"
    local tag="$2"

    # Create the new tags string
    local new_tags_str="[\"$tag\"]"

    # Use sed to replace the Tags field
    local content_after_header=$(echo "$draft_content" | sed "s/Tags: \[\]/Tags: $new_tags_str/")

    echo "$content_after_header"
}

# Function to add Monero wallet information to the draft content
add_monero_wallet_info() {
    local draft_content="$1"
    local monero_wallet="$2"
    local wallet_info="\n\nMonero Wallet Address (donations are welcome): $monero_wallet\n"
    echo -e "$draft_content$wallet_info"
}

# Function to sign the draft content with GPG
sign_draft_with_gpg() {
    local draft_path="$1"
    local draft_content="$2"

    # Write the draft content to a temporary file
    local temp_draft_path="${draft_path}.tmp"
    echo "$draft_content" > "$temp_draft_path"

    # Sign the temporary file
    #gpg --armor --detach-sign "$temp_draft_path"

    # Muting output
    gpg --armor --detach-sign "$temp_draft_path" > /dev/null 2>&1
    
    # Check if the signing was successful
    if [ $? -ne 0 ]; then
        echo "Error: Failed to sign the file with GPG." >&2
        exit 1
    fi

    # Append the signature to the input file
    local signature_file="${temp_draft_path}.asc"
    echo -e "\n$(cat "$signature_file")" >> "$temp_draft_path"

    # Clean up: Remove the temporary signature file
    rm "$signature_file"

    # Read the signed content from the temporary file
    local signed_content=$(cat "$temp_draft_path")

    # Clean up: Remove the temporary file
    rm "$temp_draft_path"

    echo "$signed_content"
}

# Main script
main() {
    # Check if the correct number of arguments is provided
    if [ "$#" -ne 1 ]; then
        echo "Usage: $0 <file_number>"
        exit 1
    fi

    local file_number="$1"

    local draft_path="/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/drafts_md/${file_number}.md"
    local posted_path="/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/posted_md/${file_number}.md"

    # Check if the draft file exists
    if [ ! -f "$draft_path" ]; then
        echo "Error: Draft file '$draft_path' not found." >&2
        exit 1
    fi

    # Read the draft content
    local draft_content=$(cat "$draft_path")

    # Count words in the draft content
    local word_count=$(count_words "$draft_content")

    # Determine the tag based on word count
    local tag=$(determine_tag "$word_count")

    # Add the tag to the draft content
    draft_content=$(add_tag_to_draft "$draft_content" "$tag")

    # Add Monero wallet information to the draft content
    local monero_wallet=$(grep MONERO_WALLET_ADDRESS ../../.env | cut -d '=' -f 2)
    draft_content=$(add_monero_wallet_info "$draft_content" "$monero_wallet")

    # Sign the draft content with GPG
    local signed_content=$(sign_draft_with_gpg "$draft_path" "$draft_content")

    # Write the signed content to the posted file
    echo "$signed_content" > "$posted_path"
}

# Call the main function with the script arguments
main "$@"

