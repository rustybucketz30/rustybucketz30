use pulldown_cmark::{html, Options, Parser};
use std::fs;
use std::io;

fn main() -> io::Result<()> {
    let markdown_path = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/links/january-2025.md";
    let html_template_path = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/pages/links.html";

    let markdown = fs::read_to_string(markdown_path)?;
    let html_links = convert_markdown_to_html(&markdown);

    insert_links_into_html(html_template_path, &html_links)?;

    Ok(())
}

fn convert_markdown_to_html(markdown: &str) -> String {
    let parser = Parser::new_ext(markdown, Options::all());
    let mut html_output = String::new();
    html::push_html(&mut html_output, parser);
    html_output
}

fn insert_links_into_html(html_template_path: &str, html_links: &str) -> io::Result<()> {
    let html_content = fs::read_to_string(html_template_path)?;

    // Find the positions for the START and END tags
    let start_tag = "<!-- START -->";
    let end_tag = "<!-- END -->";
    let start_pos = html_content.find(start_tag).map(|pos| pos + start_tag.len()).unwrap_or_else(|| html_content.len());
    let end_pos = html_content.find(end_tag).unwrap_or_else(|| html_content.len());

    // Split the HTML content and insert the new HTML links
    let (start, end) = html_content.split_at(start_pos);
    let end = &end[end_pos - start_pos..];
    let new_html_content = format!("{}{}{}", start, html_links, end);

    // Write the modified HTML back to the template file
    fs::write(html_template_path, new_html_content.as_bytes())?;

    Ok(())
}

