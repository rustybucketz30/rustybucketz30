// Add the "Drop"(0-199), "Splash"(200-499), or "Torrent Tag"(500+) depending on # words,
// Add Monero Wallet address and text
// Sign with GPG
use std::env;
use std::fs;
use std::path::Path;
use regex::Regex;
use dotenv::dotenv;
use std::process::Command;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    dotenv().ok();
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: sign <file_number>");
        return Err(Box::new(std::io::Error::new(std::io::ErrorKind::InvalidInput, "Invalid arguments")));
    }

    let file_number = &args[1];
    let draft_path = format!("/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/drafts_md/{}.md", file_number);
    let posted_path = format!("/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/posted_md/{}.md", file_number);

    if !Path::new(&draft_path).exists() {
        return Err(Box::new(std::io::Error::new(std::io::ErrorKind::NotFound, "Draft file not found")));
    }

    let mut draft_content = fs::read_to_string(&draft_path)?;

    let word_count = count_words(&draft_content);
    
    let tag = determine_tag(word_count);

    draft_content = add_tag_to_draft(&draft_content, &tag)?;

    draft_content = add_monero_wallet_info(&draft_content)?;

    let signed_content = sign_draft_with_gpg(&draft_path, &draft_content)?;

    fs::write(&posted_path, signed_content)?;

    Ok(())
}

fn count_words(content: &str) -> usize {
    content.split_whitespace().count()
}

fn determine_tag(word_count: usize) -> &'static str {
    match word_count {
        0..=256 => "drop",
        257..=512 => "splash",
        _ => "torrent",
    }
}

fn add_tag_to_draft(draft_content: &str, tag: &str) -> Result<String, &'static str> {
    let re_header = Regex::new(r#"---\nTitle: "(.*?)"\nDate: "(.*?)"\nTags: \[(.*?)\]\n---"#).unwrap();

    if let Some(caps) = re_header.captures(draft_content) {
        let mut tags: Vec<String> = caps[3].split(',')
            .map(|s| s.trim().replace("\"", ""))
            .filter(|s| !s.is_empty())
            .collect();

        if !tags.contains(&tag.to_string()) {
            tags.push(tag.to_string());
        }

        let new_tags_str = tags.iter()
            .map(|t| format!("\"{}\"", t))
            .collect::<Vec<String>>()
            .join(", ");

        let new_header = format!("---\nTitle: \"{}\"\nDate: \"{}\"\nTags: [{}]\n---", &caps[1], &caps[2], new_tags_str);
        let content_after_header = re_header.replace(draft_content, new_header);

        Ok(content_after_header.to_string())
    } else {
        Err("Header not found in markdown")
    }
}

fn add_monero_wallet_info(draft_content: &str) -> Result<String, &'static str> {
    let monero_wallet = env::var("MONERO_WALLET_ADDRESS")
        .expect("Monero wallet address not found in .env file");
    let wallet_info = format!("\nMonero Wallet Address (donations are welcome): {}\n\n", monero_wallet);

    Ok(format!("{}{}", draft_content, wallet_info))
}


fn sign_draft_with_gpg(draft_path: &str, draft_content: &str) -> Result<String, Box<dyn std::error::Error>> {
    // Write the draft content to a temporary file
    let temp_draft_path = format!("{}.tmp", draft_path);
    fs::write(&temp_draft_path, draft_content)?;

    // Capture the necessary GPG-related environment variables
    let gpg_tty = env::var("GPG_TTY").unwrap_or_else(|_| "/dev/tty".to_string());
    let gpg_agent_info = env::var("GPG_AGENT_INFO").unwrap_or_else(|_| "".to_string());

    println!("GPG_TTY: {}", gpg_tty);
    println!("GPG_AGENT_INFO: {}", gpg_agent_info);

    // Call the sign.sh script to sign the temporary file
    let output = Command::new("sh")
        .arg("/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/scripts/sign.sh")
        .arg(&temp_draft_path)
        .env("GPG_TTY", gpg_tty)
        .env("GPG_AGENT_INFO", gpg_agent_info)
        .output()?;

    if !output.status.success() {
        eprintln!("Signing script failed: {}", String::from_utf8_lossy(&output.stderr));
        return Err(Box::new(std::io::Error::new(std::io::ErrorKind::Other, "Failed to sign the draft with GPG")));
    }

    // Read the signed content from the temporary file
    let signed_content = fs::read_to_string(&temp_draft_path)?;

    // Clean up: Remove the temporary file
    fs::remove_file(&temp_draft_path)?;

    Ok(signed_content)
}
