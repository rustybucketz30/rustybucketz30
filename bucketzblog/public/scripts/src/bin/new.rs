use std::env;
use std::fs::{self, File};
use std::io::{self, Write};
use std::path::Path;
use std::process::Command;

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <title>", args[0]);
        return Err(io::Error::new(io::ErrorKind::InvalidInput, "Invalid arguments"));
    }

    let title = &args[1];
    let directory = Path::new("/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/drafts_md/");
    let next_file_number = get_next_file_number(directory)?;

    let file_name = format!("{}/{:04}.md", directory.to_string_lossy(), next_file_number);
    let date = chrono::Utc::now().format("%Y-%m-%d").to_string();
    let content = format!(
        "---\nTitle: \"{}\"\nDate: \"{}\"\nTags: []\n---\n\n# {}\n_{}_\n\n<!-- START -->\n\n\n\n<!-- END -->\n",
        title, date, title, date
    );

    let mut file = File::create(&file_name)?;
    file.write_all(content.as_bytes())?;

    // Open the file with nvim, starting at line 12
    Command::new("nvim")
        .arg("+12")
        .arg(&file_name)
        .status()?;

    Ok(())
}

fn get_next_file_number(directory: &Path) -> io::Result<u32> {
    let mut max_number: i32 = -1;

    if directory.is_dir() {
        for entry in fs::read_dir(directory)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_file() && path.extension().map_or(false, |ext| ext == "md") {
                if let Some(file_stem) = path.file_stem().and_then(|f| f.to_str()) {
                    if let Ok(number) = file_stem.parse::<i32>() {
                        max_number = max_number.max(number);
                    }
                }
            }
        }
    }

    // If no files are found, start numbering from 0
    Ok(max_number as u32 + 1)
}

