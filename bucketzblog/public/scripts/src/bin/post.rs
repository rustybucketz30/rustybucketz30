use pulldown_cmark::{Parser, Options, html};
use regex::Regex;
use std::env;
use std::fs;
use std::io::{self, Write};

fn main() -> io::Result<()> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: post <file_number>");
        return Err(io::Error::new(io::ErrorKind::InvalidInput, "Invalid arguments"));
    }

    let file_number = &args[1];
    let md_path = format!("/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/posted_md/{}.md", file_number);
    let html_path = format!("/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/html/{}.html", file_number);
    let xml_path = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/feeds/rss";
    let template_path = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/template.html";
    let blog_links_path = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/pages/blog.html";

    let markdown_content = fs::read_to_string(&md_path)?;
    let (title, date, tags, body, monero_wallet, gpg_key) = parse_markdown(&markdown_content)?;

    let html_content = convert_markdown_to_html(&body);
    write_html_output(&template_path, &html_path, &title, &date, &tags, &html_content, &monero_wallet, &gpg_key)?;

    append_to_xml(&xml_path, &title, &date, &body, &file_number)?;

    append_to_blog_links(&blog_links_path, &title, &file_number)?;
    Ok(())
}

fn parse_markdown(markdown: &str) -> io::Result<(String, String, String, String, String, String)> {
    let re_header = Regex::new(r#"---\nTitle: "(.*?)"\nDate: "(.*?)"\nTags: \[(.*?)\]\n---"#).unwrap();
    let caps = re_header.captures(markdown).ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Header not found"))?;
    let title = caps[1].to_string();
    let date = caps[2].to_string();
    let tags = caps[3].to_string();

    let body_start = markdown.find("<!-- START -->").ok_or_else(|| io::Error::new(io::ErrorKind::Other, "Start tag not found"))? + "<!-- START -->".len();
    let body_end = markdown.find("<!-- END -->").ok_or_else(|| io::Error::new(io::ErrorKind::Other, "End tag not found"))?;
    let body = markdown[body_start..body_end].trim().to_string();

    let re_monero = Regex::new(r"Monero Wallet Address \(donations are welcome\): (.+)\n").unwrap();
    let monero_wallet = re_monero.captures(markdown).map_or(String::new(), |caps| caps[1].to_string());

    let re_gpg = Regex::new(r"(?s)-----BEGIN PGP SIGNATURE-----.+-----END PGP SIGNATURE-----").unwrap();
    let gpg_key = re_gpg.captures(markdown).map_or(String::new(), |caps| caps[0].to_string());

    Ok((title, date, tags, body, monero_wallet, gpg_key))
}

fn convert_markdown_to_html(markdown: &str) -> String {
    let parser = Parser::new_ext(markdown, Options::all());
    let mut html_output = String::new();
    html::push_html(&mut html_output, parser);
    html_output
}

fn write_html_output(template_path: &str, html_path: &str, title: &str, date: &str, tags: &str, html_content: &str, monero_wallet: &str, gpg_key: &str) -> io::Result<()> {
    let template = fs::read_to_string(template_path)?;

    let output = template
        .replace("$title", title)
        .replace("$date", date)
        .replace("$tags", tags)
        .replace("$body", html_content)
        .replace("$monero-wallet", monero_wallet)
        .replace("$gpg-key", gpg_key);

    let mut file = fs::File::create(html_path)?;
    file.write_all(output.as_bytes())?;

    Ok(())
}

fn append_to_xml(xml_path: &str, title: &str, date: &str, markdown_body: &str, file_number: &str) -> io::Result<()> {
    let html_body = convert_markdown_to_html(markdown_body);
    let post_id = format!("https://bucketz.wtf/posts/html/{}", file_number);

    let new_xml_entry = format!(
        "<item>\n\
            <title>{}</title>\n\
            <author>RustyBucketz30</author>\n\
            <link href=\"{}\"/>\n\
            <guid>{}</guid>\n\
            <pubDate>{}</pubDate>\n\
            <description>\n\
                <![CDATA[{}]]>\n\
            </description>\n\
        </item>\n",
        title, post_id, post_id, date, html_body
    );

    let mut xml_content = fs::read_to_string(xml_path)?;

    // Check if the post ID already exists in the XML file
    if xml_content.contains(&post_id) {
        // Regex to find the existing entry for the post
        let re = Regex::new(&format!(r"(?s)<item>.*?<id>{}</id>.*?</item>", regex::escape(&post_id))).unwrap();
        // Replace the existing entry with the new one
        xml_content = re.replace(&xml_content, new_xml_entry.as_str()).to_string();
    } else {
        // If the post ID does not exist, append the new entry
        xml_content = xml_content.replace("<!-- END BLOG POSTS -->", &format!("{}\n    <!-- END BLOG POSTS -->", new_xml_entry));
    }

    // Write the updated XML content back to the file
    fs::write(xml_path, xml_content.as_bytes())?;

    Ok(())
}
fn append_to_blog_links(blog_links_path: &str, title: &str, file_number: &str) -> io::Result<()> {
    let mut blog_html_content = fs::read_to_string(blog_links_path)?;

    let new_link = format!(
        "<a href=\"https://bucketz.wtf/posts/html/{}\">{} - {}</a>",
        file_number, file_number, title
    );

    // Insert new link in the HTML content
    let updated_html_content = blog_html_content.replace("<!-- START LINKS -->", &format!("<!-- START LINKS -->\n\t\t\t{}", new_link));

    // Write updated content back to the HTML file
    fs::write(blog_links_path, updated_html_content.as_bytes())?;

    Ok(())
}
