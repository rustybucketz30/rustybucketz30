use std::fs;
use std::path::Path;

fn main() {
    let drafts_md_dir = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/drafts_md/";
    let posted_md_dir = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/posted_md/";
    let html_dir = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/html/";

    let drafts_md_posts = get_recent_files(drafts_md_dir);
    let posted_md_posts = get_recent_files(posted_md_dir);
    let html_posts = get_recent_files(html_dir);

    println!("{:<20} | {:<20} | {:<20}", "Drafts", "Posted", "HTML Posts");
    println!("{:-<63}", "");

    for i in 0..5 {
        let draft_post = drafts_md_posts.get(i).map_or("", |s| s.as_str());
        let posted_post = posted_md_posts.get(i).map_or("", |s| s.as_str());
        let html_post = html_posts.get(i).map_or("", |s| s.as_str());

        // Emphasize the most recent entry
        if i == 0 {
            println!("{:<20} | {:<20} | {:<20}", format!("*{}*", draft_post), format!("*{}*", posted_post), format!("*{}*", html_post));
        } else {
            println!("{:<20} | {:<20} | {:<20}", draft_post, posted_post, html_post);
        }
    }
}

fn get_recent_files(dir: &str) -> Vec<String> {
    let read_dir = match fs::read_dir(Path::new(dir)) {
        Ok(read_dir) => read_dir,
        Err(e) => {
            eprintln!("Failed to read directory '{}': {}", dir, e);
            return Vec::new();
        }
    };

    let mut files: Vec<_> = read_dir
        .filter_map(|entry| {
            entry.ok()
                .and_then(|e| e.path().file_stem().map(|s| s.to_string_lossy().into_owned()))
        })
        .collect();

    files.sort_by(|a, b| b.cmp(a)); // Sort in descending order
    files.truncate(5); // Keep only the 5 most recent
    files
}
