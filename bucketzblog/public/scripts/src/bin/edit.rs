use std::env;
use std::path::Path;
use std::process::Command;

fn main() {
    let markdown_dir = "/mnt/00_root/00_Root/01_RustyBucketz30/RustyBucketz30/bucketzblog/public/posts/markdown/";
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        eprintln!("Usage: edit <post_number>");
        return;
    }

    let post_number = &args[1];
    let file_path = format!("{}{}.md", markdown_dir, post_number);

    // Check if the file exists
    if !Path::new(&file_path).exists() {
        eprintln!("No blog post found with number: {}", post_number);
        return;
    }

    // Open the file in nvim
    match Command::new("nvim")
        .arg("+12")
        .arg(file_path)
        .status() {
            Ok(status) if status.success() => {},
            _ => eprintln!("Failed to open the file in nvim."),
    }
}

