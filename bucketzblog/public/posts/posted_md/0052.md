---
Title: "Counter-culture vs. anti-culture"
Date: "2024-05-17"
Tags: ["splash"]
---

# Counter-culture vs. anti-culture
_2024-05-17_

<!-- START -->

I don't wish to pretend my _Luddite-ian_ opinions on technology and **powerful** ambivalence toward politics (and the zeitgeist) are novel, they most certainly are not. But perhaps I can tidy up a misnomer where one may consider myself a 'counter-culturalist', meaning that I am someone who goes against the culture and makes that their own culture; or some playful derivative on a 'contrarian'.

This I think is inaccurate, and not in a nitpick-y way but fundamentally - Because there is, and always will be a place for counter-culture within culture, it's partially what makes it culture itself in the first place. But I posit that 'counter-culture' and 'culture' itself are both sides of the same coin. Both existing within the larger societal **'system'** that produces both culture and an increasingly robust system (and counter-system) in and of itself, ending in a ouroboros, positive feedback loop all making one giant machine. An _'anti-culturalist'_ is more fundamentally against the concept of culture altogether and puts more faith into an individual making autonomous decisions about what to spend one's life doing, what to value, what to watch, buy, do (curating own inputs & outputs, but mostly removing inputs and increasing outputs if bothering to do it at all). Counter-culturalists may fight for representation at the table, whereas anti-culturalists prefer to eat and fare alone, or with fellow individuals.

Perhaps it is impossible to exist entirely outside the 'cultural system' and an 'anti-culturalist' is merely some forgettable subset of counter-culturalists but I would still turn a nose up at being called a counter-culturalist at all, given they have as much emotional dependance on the system as pure culturalists do. I'll submit that there isn't a way to financially (nutritionally - more fundamentally, I suppose) to be truly soverign in our current world save having an amount of presumably hidden money that wouldn't make you a John McAfee counter-culturalist by default but I would seperate emotional, or material attachment to a system as an okay measure of cultural (or counter-cultural) dependancy, which I may seek to reduce.

Maybe I wish that my version of 'anti-culture' would become the true 'counter-culture' with its own place in the _'system'_, but I cannot feign even this discrepancy.

So fuck culture, and counter-culture. And fuck me too, I guess.

<!-- END -->

Monero Wallet Address (donations are welcome): 84CAPQe8aqUQLVcnr91mKtiEvuveckDWCRNgCVSTmEhS59QUwv8HFe9PsuBGCeYRfx3ceBYnjMLxkUupGkWgbRdtDwAry6H

-----BEGIN PGP SIGNATURE-----

iQEzBAABCAAdFiEE7vfxUPpoBFKucVEsAadRWO+f34AFAmZG2NYACgkQAadRWO+f
34BUKwf/dGYfF3X3KEgjZfg6G28sVnGlrDvfdAU/zSdrLbs7KwUfcB+flPKkXkFn
vfeUrWG/pA6tb89bwyDrDbhXrXt8viMd3eFwYwTRKGBwezli2la33MGvUhbNpR1j
pFj+ZG0ghvZrt/5Jt22X94hIqCMOY1YsCXLSpl5oWbHAxXlyut/ZczC5MBxe7Ydp
kWuRyPOgaqrUmJO3os9L5/aRXRjJZVcWLJtjwsGLpoPL0vk9MG1i0Et+S8htKr6J
Zfd7aBqiRD5fG7mD0yzjNWOCF0OBOxtRJ+1Xk3ojf7rJRHE89ba8kRMIXiS2x0va
yVyNB42YxuMOme3o3ma2KwXMhUx5yA==
=xupu
-----END PGP SIGNATURE-----
