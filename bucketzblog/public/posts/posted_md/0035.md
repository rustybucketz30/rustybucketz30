---
Title: "On work"
Date: "2024-03-20"
Tags: ["splash"]
---

# On work
_2024-03-20_

<!-- START -->

What is _work_ to you?

Many of us are always _'working'_. Which I find offensive. Given I almost never define said work in even a similar way to which the other is referring to it. 

On a whim, I was able to generate many definitons of this nebulous **'work'**, non-exhaustive of course:

 - vaguely, the hours of 8am to 5pm, Monday thru Friday, in one's respective time zone
 - the de facto and _excellent_ excuse for not attending, or involving ones self in an undesirable affair
 - whatever one does for money; a profession; a career; employment; 'what bossman says to do'
 - a performance of sorts, where an appearance of _busy-ness_ percludes any need of sending, much less reading emails or Teams messages
 - things that 'are to be done', but are not necessarily enjoyable; laundry, lawn care, hygiene (to some)
 - unbounded, uncontrained, and solitary time used to explore, create, or ruminate - embracing any difficulty or uncomfortability that ensues  
 - sustained physical and/or mental time & effort exerted toward the realization of one's potential or any particular goal, perhaps conceptual, material, or just merely symbolic of the former or latter
 - lastly, perhaps on one's worst days: _life_ (life is work, to work is to be alive)

_As an aside, I for one, love doing laundry_

So, how do I know what anyone is doing when they are '**working**'? One thing is certain: working is not inherently virtuous or viceful. Your definition of it may make it so.

How could something so central to our daily affairs and existence, be so intenible? Almost meaningless?

Not sure, but you'll find me working...

<!-- END -->

Monero Wallet Address (donations are welcome): 84CAPQe8aqUQLVcnr91mKtiEvuveckDWCRNgCVSTmEhS59QUwv8HFe9PsuBGCeYRfx3ceBYnjMLxkUupGkWgbRdtDwAry6H

-----BEGIN PGP SIGNATURE-----

iQEzBAABCAAdFiEE7vfxUPpoBFKucVEsAadRWO+f34AFAmX7B5sACgkQAadRWO+f
34BS0Qf/T5XdirakJMmXSu/QirpHpUhPQ/lGB2gyJG6aZDeFIG66HN8tgbKGqu3U
Nsr2IOe+xoMY9fytHpSX4h1jCVQ89RIPkA/cM6DfB833ukluZuk4QsOIHxzqoCmm
HUWtdOznCqE/1DwD96gzJX6uBLbTMGJbVVmgxrATuN/M+zk4VS1TZLm7DlRhRlpm
HSHC0PaGm9fKDjL7kHho13EZhU/sCgDHw2NdIpuzGyrMc7NrANkij5E1rkcHZ90F
yaJnBs+8curdZ6sl0bpwgVCZUZ6FKYYYHnbCgMo3zRLRg6MitzJ5Eh2ZHOzDoIST
wuHkLdlO6/wTSF9nwzOyIfEelQiAdw==
=MsVq
-----END PGP SIGNATURE-----
