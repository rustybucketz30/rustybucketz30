---
Title: "Going mad"
Date: "2024-11-11"
Tags: ["drop"]
---

# Going mad
_2024-11-11_

<!-- START -->

Living in a world beyond our ancestors reckoning, are we living in their wildest dreams or worst of nightmares? Has the flame of their campfire gone out too soon leaving them in a cold, feverish state or did the flame engulf them in their slumber and our moment in time the inferno of all nine circles at once.

Our culture of anti-balance should leave questions as to why more are not mal-adjusted to their sterile, corporate environments. Perhaps most are testing their ability to adapt to even the most devilish of conditions, or are just skilled at imagining affairs being far worse. History scaring modern man into conformity and dreams of lesser nightmares. The fundamentals of their our species' existence boiled down to comfort through consumption... a pithy contradiction recited as a tautology that leaves a man of the mean in a perpetual paradox of what he is to do. 

To go against the zeitgeist's monopoly on values is to be a moral terrorist, fought not by sword but the perpetual barrage of skepticism and temptation to rejoin the fray, lest you go mad. 

I don't ask to transcend purpose. But _divine being_; God; Fortuna: Let me go mad with pen in hand!

<!-- END -->

Monero Wallet Address (donations are welcome): 84CAPQe8aqUQLVcnr91mKtiEvuveckDWCRNgCVSTmEhS59QUwv8HFe9PsuBGCeYRfx3ceBYnjMLxkUupGkWgbRdtDwAry6H

-----BEGIN PGP SIGNATURE-----

iQIzBAABCAAdFiEEvV/lPIdcjM2khatzKvNmRNzIUNoFAmcyKpMACgkQKvNmRNzI
UNrlrQ/8DPPFWn0dNo91lNqJTcjZcaghlLPNGRVsczmdTVcWSLCGV/utKp8Qriwb
3cJLYCdmEUI78rZS7yullgBV2B0lFRs7wX0SUcqDV30TMq2zjAnzvp5Gn9hksSAa
HfLRXDeeN2iMdRQWPJgPDgZPRvmrkxJTFws10a9pCe7J0TdaYEigKimLuDHvYiey
CcWRyf1vl+oCEz4noLQmnft0XbeQwp69ySVhapDtezFodUfwuipfaJ+Gm6tL0i/5
Haw2sq6epP7kqzdg98ojEffsg8x06dHe2IxLbgq1osu5goS4UGcwvMt8WKx2qsL4
jVxJwKGbyhxUnLriosDiHUwqfUjUvDBbbp1UWCSWPgQaBcNbstEfH7Jc6Zk1Bn1o
IfNc1A+LGHsXOaQlFXDzTcFi4PX35TE7gnoXi6BATLouQdK8UJJdfL5XUW/KPREF
aP6SRhyxazYBqAWAYDHZq9kxWpD/MzzVOc/0FHOdBqv4WmHA4bgOwo9HPS7OlJYg
dX0cQQ3ZDe2dChkCzL+T+60gT4olvrC0VvBuFKELPVLzAL7uyUgqgff1HPTH59jb
0vUxkwvVIBgd5QrfZi1iKMpHldgI4SEvtIAB6tlza+Sh4Fu9ggAJRTz4NMbKouOC
3sQVatc4NoZ9HL/t6woasFdB3xOJktyxwaxQWobqqdAbVkEo5ho=
=x1Dt
-----END PGP SIGNATURE-----
