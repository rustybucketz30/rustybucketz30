---
Title: "A Today day"
Date: "2024-03-08"
Tags: ["splash"]
---

# A Today day
_2024-03-08_

<!-- START -->

What are doing today? Beyond your enumerated list of TODOs, I wager that you are merely thinking of tomorrow, or **a** tomorrow. Or, maybe today you thought mostly of yesterday, only to remind yourself that yesterday is gone. But tomorrow is still there!

But what about today? Not every day can be lived **today**, sometimes a *tomorrow's* today behooves you, or possibly even a *yesterday's* today. Too much *today's* today we may lose perspective, but too little and we lose life. Only but a day at a time.

Some take this to mean the proper *today's* today is the current day only when celebration or assured joy is in order, which I say is not so. That is merely hedonism, abstracted and timed during many *tomorrow's* todays. A *today's* today is **presence**. Perhaps to an extent of the loss of focus, in lieu of pure presence.

I will say that a *today's* today isn't **just** there for you tomorrow. Today may be pain, meant to be shouldered only today. So bear it now. Do not dwell in the yesteryear of pain or happiness alike, or make shade under the forecasted doom or comfort that may lie ahead.

Perhaps one must void hope and memory both to bear life presently. But memories burn too strong, and hope to powerful a drug. So we mosey on. Living mostly *tomorrow's* today - and to that, I hope today comes for you, and you accept it. Whenever that may be.

<!-- END -->

Monero Wallet Address (donations are welcome): 84CAPQe8aqUQLVcnr91mKtiEvuveckDWCRNgCVSTmEhS59QUwv8HFe9PsuBGCeYRfx3ceBYnjMLxkUupGkWgbRdtDwAry6H

-----BEGIN PGP SIGNATURE-----

iQEzBAABCAAdFiEE7vfxUPpoBFKucVEsAadRWO+f34AFAmXrkb8ACgkQAadRWO+f
34B4MQf/dnKyqhaCBzdnmHBtJa91+itO9v64VNcunacrixZtI6hdj1d3Gwa8Z8n6
sra3zR0Q+gggtIzA+jsYyI6B4xeUZND7tV5cT5rZEi9wDBxHx+KBgbwKRghLL0ve
rCY0iPfeJamjFABvNyZqp7U3zj6pG32gv53notUb3E0kFRX1vhbPVeO1dxhTiAmY
WHbhCygMJOYrUH2z3Pj7jHcyVrg50wB5HQT5iGDxnaLdBtMzaWnirzymRnxkq4ub
V0WVtm3LRaQzEjQnsseLAgytZSSXFD+vDGYWEynDt8JlPj4D4w/MGD2y3kIEAiEA
S0KBU5yLTn450yaweTOSigjS4gh/nQ==
=oYvR
-----END PGP SIGNATURE-----
