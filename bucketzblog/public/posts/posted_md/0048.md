---
Title: "My Schtick"
Date: "2024-04-15"
Tags: ["drop"]
---

# My Schtick
_2024-04-15_

<!-- START -->

Most people have a _schtick_, I bastardize the meaning a bit here but its what people **do**. Do with their time, their minds, and their hands.

Rock climbing, money, sedation. To each their own, but a _schtick_ nonetheless.

Mine? Mine is just a question: **why?**

Why does this work the way it does?

Why do I do what I do? Why do you?

Why am I alive? I'm not ungrateful, just curious...

It seems to harmonize all my musings into just a simple question: _"why tho?"_

<!-- END -->

Monero Wallet Address (donations are welcome): 84CAPQe8aqUQLVcnr91mKtiEvuveckDWCRNgCVSTmEhS59QUwv8HFe9PsuBGCeYRfx3ceBYnjMLxkUupGkWgbRdtDwAry6H

-----BEGIN PGP SIGNATURE-----

iQEzBAABCAAdFiEE7vfxUPpoBFKucVEsAadRWO+f34AFAmYdQYkACgkQAadRWO+f
34DwUQf+OwfNEuJJ0G3PsTx0OSve/yfaK+DXMNDjjLpRQOqeCjv64cd7hHtNDRg3
S443jWIIHJYyGHYYkBqo6l7qA18LI9WE+X1cZ6Mn7JY3+vG5zLTZ3B2lQxV7WZo3
KEkjTckQtlAxJ/bcAD//kO8iofYxZKzliPHICqP0RGyslOxBakumDgg0S7lCQ0RT
zXcRBmsFWkLScfODDP4MYZrS6byRKK5DVT4XOj6JtaOgdO4yF7CYVEaNCYC6vpaX
9H9MHxUpX336mrgEL3wVTELo83rRRgBGy/D0nPLUFDp492+4SMLOnmDP/SrazqxV
kTCMaRdCZ0RNNxdJrNpKZabFWLDYOQ==
=Pccw
-----END PGP SIGNATURE-----
