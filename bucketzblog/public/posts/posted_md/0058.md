---
Title: "Contempt for modern man"
Date: "2024-06-09"
Tags: ["splash"]
---

# Contempt for modern man
_2024-06-09_

<!-- START -->

A question I find circling oft inside my head:

**Why do I have such contempt with modern man? What about our current lives, at the individual and societal level, do I have so much beef with?**

The reactive counter-question of "_Should I?_", is one I won't entertain at length now as I find difficulty in sympathizing with any view where the daily habits of a modern man is something that deserves applause. Of the individual of course, not the species.

Regardless, my general sentiment around this contempt are in our failures to harmonize any individual peace with that of fellow man, and the world; both its material and immaterial. And instead to arrogantly dismiss virtue, courting our neuroses and corrupted senses to insult the organs that make up our bodies while parading thoughtless products and lifestyles that worship our power over the earth beneath our feet. 

With all our evidence of genius and procurement of abundance, it is still not enough for one to be simply be a good and virtuous man. You must be deserving of prosperity, earning one's own existence saving no expense of others or of the sanctity of your own. Not to be one with oneself, but only one amongst billions and to never stray from that fact. 

In both the rich man and the poor man, I see a coward. Slaves to vices, impulses, empty ambitions that lay bare to fellow man's equally bare ambitions. Taking expenses of one another, nature, and any _God_. Husks of men that joyfully carry out their hostile pittances guilt-free - and relishing in the pain they inflinct onto anything outside of themselves. Ignorant too, of which is which. Yet with such audacity to confidently challenge wisdom for its purpose and life for its power.

If the only constant state of in our existence is flux, surely we can change to be better.
<!-- END -->

Monero Wallet Address (donations are welcome): 84CAPQe8aqUQLVcnr91mKtiEvuveckDWCRNgCVSTmEhS59QUwv8HFe9PsuBGCeYRfx3ceBYnjMLxkUupGkWgbRdtDwAry6H

-----BEGIN PGP SIGNATURE-----

iQEzBAABCAAdFiEE7vfxUPpoBFKucVEsAadRWO+f34AFAmZmL+cACgkQAadRWO+f
34CgIwgAuCq1x9VjDEUXFu1OE6J6m10v6T6pC3xannfSeBJBue2t66bkqNGxgAle
aD3U75n4tmEalcmVXs6l4qecKCmKXMO/mn30nlDGmlXqLON7Q1NM8OV722YXnDps
gDquF7xrG8UJNNXWo0kDVR/qFUprmAIkeG+eso7OvvNmVsX/YGurJNcSORp0rr6b
q+4ttAVT4Pgr/tuiON9vgJnoriY+OLozV9dxJtd+GSl3S9KmI3yxwLOHy4y+0eW5
dRS1zrvUnxdMTeFQoSNcWoGzLoplRWUiEyyGEAFj0W6dyc2KC8q6l7rDCVUK+nvJ
VUqxLWZGts9Ycnle0U6sEH1OvP465w==
=z4mM
-----END PGP SIGNATURE-----
