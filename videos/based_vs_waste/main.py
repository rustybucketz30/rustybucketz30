import pygame
import os

# Initialize PyGame
pygame.init()

# Set the dimensions of the window
window_width, window_height = 1800, 800
window = pygame.display.set_mode((window_width, window_height))

# Set a title for the window
pygame.display.set_caption("Based or Waste Game")

# Define colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Set up font for text
font_size = 36
font = pygame.font.Font(None, font_size)

# Mini logo width
mini_logo_width = 90

# Lists to store mini logos and their positions
based_mini_logos = []
waste_mini_logos = []

# Function to create a mini logo with aspect ratio preserved
def create_mini_logo(path):
    image = pygame.image.load(path)

    # Calculate the new height to maintain the aspect ratio
    aspect_ratio = image.get_width() / image.get_height()
    mini_logo_height = int(mini_logo_width / aspect_ratio)

    # Scale the image to the new dimensions
    return pygame.transform.scale(image, (mini_logo_width, mini_logo_height))

# Function to read items from list.md
def read_items(file_path):
    with open(file_path, 'r') as file:
        items = [line.strip().split(' - ') for line in file]
    return items

# Load items
items = read_items('list.md')
item_index = 0

# Load images with scaling and error handling
def load_image(path, is_graph=False):
    if os.path.exists(path):
        image = pygame.image.load(path)
        if is_graph:
            # No need to scale as dimensions are already correct
            return image
        else:
            image_height = 300
            aspect_ratio = image.get_width() / image.get_height()
            image_width = int(image_height * aspect_ratio)
            return pygame.transform.scale(image, (image_width, image_height))
    else:
        print(f"Error: File {path} not found.")
        return None

# Load the graph image
graph_image = load_image('based_waste_graph_empty.png', is_graph=True)
# Position to fill the right 2/3 of the screen
graph_position = (window_width - 1200, 0)

# Text input box settings
left_column_x = window_width // 6
input_box_width = 140
input_box = pygame.Rect(left_column_x - input_box_width // 2, 650, input_box_width, 32)  # Centered below logo
user_text = ''
color_inactive = pygame.Color('lightskyblue3')
color_active = pygame.Color('dodgerblue2')
color = color_inactive
active = False

# Load the first item image
current_image = load_image(items[item_index][1])
image_position = (left_column_x, 400)  # Position to the left

# Variables to track positions for mini logos
based_y, waste_y = 0, 0  # Starting Y positions for mini logos

# Game Loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if active:
                if event.key == pygame.K_RETURN:
                    if user_text in ['BASED', 'WASTE']:
                        # Create mini logo before incrementing item_index
                        mini_logo = create_mini_logo(items[item_index][1])
                        position = (window_width - 150, based_y) if user_text == 'BASED' else (650, waste_y)
                        if user_text == 'BASED':
                            based_mini_logos.append((mini_logo, position))
                            based_y += 90  # Increment position for the next BASED logo
                        else:
                            waste_mini_logos.append((mini_logo, position))
                            waste_y += 90  # Increment position for the next WASTE logo
                        
                        item_index += 1
                        if item_index < len(items):
                            current_image = load_image(items[item_index][1])
                        else:
                            current_image = None
                        user_text = ''  # Reset text for next item
                elif event.key == pygame.K_BACKSPACE:
                    user_text = user_text[:-1]
                else:
                    user_text += event.unicode
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if input_box.collidepoint(event.pos):
                active = not active
            else:
                active = False
            color = color_active if active else color_inactive

    # Fill the background with black
    window.fill(BLACK)

    # Blit the graph image on the right side
    if graph_image:
        window.blit(graph_image, graph_position)

    # Display the current item as white text on the left side
    if item_index < len(items):
        os_name = items[item_index][0]
        text_surface = font.render(os_name, True, WHITE)
        text_rect = text_surface.get_rect(center=(left_column_x, 50))
        window.blit(text_surface, text_rect) 
    
    # Display mini logos
    for logo, position in based_mini_logos:
        window.blit(logo, position)
    for logo, position in waste_mini_logos:
        window.blit(logo, position)

    # Blit the current image on the left side
    if current_image:
        image_rect = current_image.get_rect(center=image_position)
        window.blit(current_image, image_rect)

    # Render the current user_text centered below the logo
    txt_surface = font.render(user_text, True, color)
    txt_rect = txt_surface.get_rect(center=(left_column_x, input_box.y + input_box.height // 2))
    window.blit(txt_surface, txt_rect.topleft)
    pygame.draw.rect(window, color, input_box, 2)

    # Update the display
    pygame.display.flip()

    # Check if we've reached the end of the items
    if item_index >= len(items):
        running = False

# Quit PyGame
pygame.quit()
