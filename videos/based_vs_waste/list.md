1. Arch - pngs/arch.png
2. Gentoo - pngs/gentoo.png
3. LFS - pngs/lfs.png
4. MacOS - pngs/macos.png
5. iOS - pngs/ios.png
6. Red Hat Enterprise Linux - pngs/redhat.png
7. OpenBSD - pngs/openbsd.png
8. NixOS - pngs/nixos.png
9. OpenSUSE - pngs/opensuse.png
10. ChromeOS - pngs/chromeos.png
11. Android - pngs/android.png
12. Libre Android Forks (GrapheneOS, LineageOS) - pngs/libreandroid.png
13. Windows - pngs/windows.png
14. All Debian/Ubuntu-based Distros - pngs/debianubuntu.png
15. TempleOS - pngs/templeos.png
16. RedStarOS - pngs/redstaros.png

