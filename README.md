# RustyBucketz30 Monorepo
For all things RustyBucketz30. Libre-licensed for you. Explore at your leisure

## Directory Structure
```
## Project Structure
 - bucketzblog/        # A Drop From The Bucket (Blog)
 - backups/            # Minecraft World Backups (2)
 - dotfiles/           # Hyprland on Arch Linux configs
 - secret/             # Dev clout booster
 - videos/             # Memes & video mats for YT Vids
    - based_vs_waste/
    - libre_licenses/
 - LICENSE
 - README.md
```

## A Drop From The Bucket (RustyBucketz30's Blog)
This is my personal indie tech blog. Made with Rust scripting and Markdown - details in the README inside.

## Minecraft Let's Play World Backups
I will generally try to keep 2 backups of my Minecraft Let's Play World in here. It is huge, so I'll have to move this off or self-host the files eventally. 

## Dotfiles for Hyprland on Arch Linux Setup
There are my desktop configurations, detailed info is in the README in the directory.

## License
Do whatever you want with this, just don't be a dick.

